import com.clarolab.selenium.pages.browser.LocalBrowserBuilder;
import com.clarolab.selenium.pages.browser.web.WebBrowser;
import com.clarolab.selenium.pages.exception.FrameworkWebDriverException;
import com.clarolab.selenium.pages.pages.TopLevelPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ExampleTest {

    WebBrowser browser;

    // Download chromedriver from https://chromedriver.chromium.org/downloads
    // Set your browser path at Properties.browserPath

    // region Private methods
    protected WebBrowser createMinimalChrome() throws FrameworkWebDriverException {
        return LocalBrowserBuilder.getChromeBuilder(Properties.url)
                .withWebDriverPath(Properties.browserPath)
                .build();
    }

    @BeforeMethod(alwaysRun = true)
    protected void initTest() throws FrameworkWebDriverException {
        browser = createMinimalChrome();
        getBrowser().openPageByURL(Properties.url);
    }

    @AfterMethod(alwaysRun = true)
    protected void cleanTest() {
        getBrowser().cleanSession();
        getBrowser().quit();
    }

    protected WebBrowser getBrowser(){
        return browser;
    }

    public  <T extends TopLevelPage> T loadTopPage(Class<T> pageClass){
        return getBrowser().loadTopLevelPage(pageClass);
    }
    // endregion

    // This test looks for "clarolab" and verify that it is the first item to appear
    @Test
    public void firstTestExample() {
        GooglePage page = loadTopPage(GooglePage.class);
        GoogleResultsPage googleResultsPage = page.searchAndGo("clarolab");
        googleResultsPage.verifyFirstItemIs("www.clarolab.com");
    }
}
