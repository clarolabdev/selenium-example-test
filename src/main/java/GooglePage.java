import com.clarolab.selenium.pages.browser.web.WebBrowser;
import com.clarolab.selenium.pages.pages.BaseTopLevelPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class GooglePage extends BaseTopLevelPage {

    private static final By MAIN_PAGE_ID = By.id("searchform");
    private final By SEARCH_BOX = By.name("q");

    @Override
    public By getPageIdentifier() {
        return MAIN_PAGE_ID;
    }

    public void setTestUrl(String url) {
        ((WebBrowser)a.getBrowser()).openPageByURL(url);
    }

    public GoogleResultsPage searchAndGo(String searchTerm) {
        a.clearText(SEARCH_BOX);
        a.inputTextSlowly(SEARCH_BOX, searchTerm);
        a.getElement(SEARCH_BOX).sendKeys(Keys.ENTER);
        return a.loadTopLevelPage(GoogleResultsPage.class);
    }
}