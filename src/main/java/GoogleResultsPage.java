import com.clarolab.selenium.pages.browser.web.WebBrowser;
import com.clarolab.selenium.pages.config.TimeoutType;
import com.clarolab.selenium.pages.pages.BaseTopLevelPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.List;

public class GoogleResultsPage extends BaseTopLevelPage {

    private static final By MAIN_PAGE_ID = By.id("searchform");
    private final By SEARCH_RESULT_ITEMS = By.className("g");

    @Override
    public By getPageIdentifier() {
        return MAIN_PAGE_ID;
    }

    public void verifyFirstItemIs(String term) {
        List<WebElement> results = a.getElements(SEARCH_RESULT_ITEMS);
        String textContent = results.get(0).getAttribute("textContent");
        Assert.assertTrue(textContent.contains(term));
    }

}
